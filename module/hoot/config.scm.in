;;; WebAssembly compiler
;;; Copyright (C) 2023 David Thompson <dave@spritely.institute>
;;; Copyright (C) 2024 Andy Wingo <wingo@igalia.com>
;;;
;;; Licensed under the Apache License, Version 2.0 (the "License");
;;; you may not use this file except in compliance with the License.
;;; You may obtain a copy of the License at
;;;
;;;    http://www.apache.org/licenses/LICENSE-2.0
;;;
;;; Unless required by applicable law or agreed to in writing, software
;;; distributed under the License is distributed on an "AS IS" BASIS,
;;; WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
;;; See the License for the specific language governing permissions and
;;; limitations under the License.

;;; Commentary:
;;;
;;; Build time configuration.
;;;
;;; Code:

(define-module (hoot config)
  #:export (%version
            %js-runner-dir
            %reflect-js-dir
            %reflect-wasm-dir
            %stdlib-path
            %node
            %d8))

(define %version "@PACKAGE_VERSION@")
(define %datadir "@hoot_datadir@/@PACKAGE@/@PACKAGE_VERSION@")

(define %js-runner-dir
  (or (getenv "HOOT_JS_RUNNER_DIR")
      (in-vicinity %datadir "js-runner")))

(define %reflect-js-dir
  (or (getenv "HOOT_REFLECT_JS_DIR")
      (in-vicinity %datadir "reflect-js")))

(define %reflect-wasm-dir
  (or (getenv "HOOT_REFLECT_WASM_DIR")
      (in-vicinity %datadir "reflect-wasm")))

(define %stdlib-path
  (cond
   ((getenv "HOOT_STDLIB_PATH") => parse-path)
   (else (list (in-vicinity %datadir "lib")))))

(define (false-if-empty str) (if (string-null? str) #f str))
(define %node (false-if-empty "@NODE@"))
(define %d8 (false-if-empty "@D8@"))
